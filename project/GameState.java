import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class GameState extends World
{
    int totalHugs = 0;
    
    Player player;
    public GameState()
    {    
        super(800, 600, 1); 
        AddRandomCats();
        player = new Player();
        addObject( player, 400, 300 );
    }
    
    public void act() 
    {
        showText( "Hugs: " + totalHugs, 75, 30 );
    }
    
    public void IncrementHugs()
    {
        totalHugs += 1;
    }
    
    public int GetPlayerX()
    {
        return player.getX();
    }
    
    public int GetPlayerY()
    {
        return player.getY();
    }
    
    void AddRandomCats()
    {
        for ( int i = 0; i < 10; i++ )
        {
            int x = Greenfoot.getRandomNumber(800);
            int y = Greenfoot.getRandomNumber(600);
            int type = Greenfoot.getRandomNumber(2);
            
            if ( type == 0 )
            {
                addObject( new OrangeCat(), x, y );
            }
            else if ( type == 1 )
            {
                addObject( new SitCat(), x, y );
            }
        }
    }
}
