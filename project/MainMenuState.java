import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class MainMenuState extends World
{
    public MainMenuState()
    {    
        super(800, 600, 1); 
        GameState state = new GameState();
        Greenfoot.setWorld( state );
    }
}
