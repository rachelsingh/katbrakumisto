import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Player extends Actor
{
    int speed = 4;
    boolean isHugging = false;
    int hugTimer = 0;
    
    public void act() 
    {
        HandleKeyboard();
        UpdateTimer();
    }
    
    void UpdateTimer()
    {
        if ( hugTimer > 0 )
        {
            hugTimer = hugTimer - 1;
            if ( hugTimer == 0 )
            {
                isHugging = false;
                setImage( "player_down.png" );
            }
        }
    }
    
    void HandleKeyboard()
    {
        int x = getX();
        int y = getY();
        
        if ( Greenfoot.isKeyDown( "up" ) )
        {
            y = y - speed;
        }
        else if ( Greenfoot.isKeyDown( "down" ) )
        {
            y = y + speed;
        }
        
        if ( Greenfoot.isKeyDown( "left" ) )
        {
            x = x - speed;
        }
        else if ( Greenfoot.isKeyDown( "right" ) )
        {
            x = x + speed;
        }
        
        if ( Greenfoot.isKeyDown( "space" ) )
        {
            Hug();
        }
        
        setLocation( x, y );
    }
    
    void Hug()
    {
        if ( IsHugging() == true )
        {
            return;
        }
        
        OrangeCat oc = (OrangeCat)getOneIntersectingObject( OrangeCat.class );
        if ( oc != null )
        {
            isHugging = true;
            hugTimer = 20;
            setImage( "player_hug.png" );
            oc.BeingHugged();
            Greenfoot.playSound( "meow.wav" );
        }
        
        SitCat sc = (SitCat)getOneIntersectingObject( SitCat.class );
        if ( sc != null )
        {
            isHugging = true;
            hugTimer = 20;
            setImage( "player_hug.png" );
            sc.BeingHugged();
            Greenfoot.playSound( "meow.wav" );
        }
        
        if ( isHugging )
        {
            GameState gs = (GameState)getWorld();
            gs.IncrementHugs();
        }
    }
    
    boolean IsHugging()
    {
        return isHugging;
    }
}
