import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Cat extends Actor
{
    protected int hugTimer = 0;
    
    public void act() 
    {
        UpdateTimer();
    }
    
    void UpdateTimer()
    {
        if ( hugTimer > 0 )
        {
            hugTimer = hugTimer - 1;
            if ( hugTimer == 0 )
            {
                GameState state = (GameState)getWorld();
                setImage( "orangecat_lay.png" );
                setLocation( state.GetPlayerX(), state.GetPlayerY() );
            }
        }
    }
    
    public void BeingHugged()
    {
        setImage( "empty.png" );
        hugTimer = 20;
    }
}
